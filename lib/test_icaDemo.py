# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 21:31:11 2021

@author: David
"""

import os
from wavFunctions import findWavFiles


def test_sampleFiles():
    '''Tests to ensure sufficient sample files are present'''

    cwd = os.getcwd()

    # Negative check: ensure findWavFiles is written correctly
    # (should find no wav files in code)
    codeFolder = os.path.join(cwd, "code/")
    assert findWavFiles(codeFolder) == 0

    # Positive check: ensure there are sufficient wav files in the resources folder.
    resourceFolder = os.path.join(cwd, "resources/")
    assert findWavFiles(resourceFolder) > 3

# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 22:14:46 2021.

@author: David
"""

import os


def findWavFiles(curFolder):
    """Return a list of all wav files directly within a folder."""
    filesInFolder = os.listdir(curFolder)
    wavFiles = []
    for curFile in filesInFolder:
        if curFile.endswith(".wav"):
            wavFiles.append(curFile)
    return wavFiles

# ICA Demo

ICA Demo is a python script which demonstrates the use of the Independent Component Analysis algorithm.

## What It Does

ICA Demo allows you to listen to several different sound files.
Then, it allows you to place them at different points in a "room".
Next, place yourself, the listener, in the same room. You'll be able to hear what the combined music would sound like.
Finally, ICA Demo will attempt to separate the sound files back out using the ICA algorithm.

## Usage

```python

python ICADemo.py

```

## To-Do
1. Code currently reads in .wav files and mashes them together equally.
2. Next step: write method which uses distances to mash together files
  2.1 How do audio files propagate over distances? 
    2.1.1 Gut feeling: decrease as the square. 
    2.1.2 Should be able to just multiply the NumPy array?
3. Script which generates several different "listeners"
  3.1 How to handle clipping outside int range?
4. Apply ICA
  4.1 https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.FastICA.html
    

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
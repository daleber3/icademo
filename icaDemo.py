# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 21:26:36 2021.

@author: David
"""

import wave
import os
import numpy as np
import sklearn

cwd = os.getcwd()
resourceFolder = os.path.join(cwd, "resources/")

filesInFolder = os.listdir(resourceFolder)
soundsHeard = []

# Read in all the .wav files and convert to numpy array.
for curFile in filesInFolder:
    if curFile.endswith(".wav"):
        print("")
        print("Found sound file:", curFile)
        fileAddress = os.path.join(resourceFolder, curFile)

        # Credit: https://stackoverflow.com/a/53978531
        print("Opening sound file.")
        wavObj = wave.open(fileAddress)
        nFrames = wavObj.getnframes()
        print("Number of frames: ", nFrames)
        frames = wavObj.readframes(nFrames)
        samples = np.frombuffer(frames, dtype="<i2")
        soundsHeard.append(samples)

# Get the shortest sound length
minLength = None
for curArray in soundsHeard:
    curLen = len(curArray)
    
    if minLength is None:
        minLength = curLen
    elif curLen < minLength:
        minLength = curLen
        
print("\nThe shortest file found was ", minLength, " long.")

# Cut all sounds to the shortest length
numSounds = len(soundsHeard)
shortSounds = np.zeros((numSounds, minLength))
for i, curArray in enumerate(soundsHeard):
    shorterArray = curArray[:minLength]
    shortSounds[i, :] = shorterArray
    
# Mix the sounds
mixedSounds = np.sum(shortSounds, axis=0)

# Write to output
outputFolder = os.path.join(cwd, "results/")
destinationFile = os.path.join(outputFolder, "test.wav")
mix_wav = wave.open(destinationFile, 'w')
oldParams = wavObj.getparams()
print(oldParams)
mix_wav.setparams(oldParams)
mix_wav.writeframes(mixedSounds.astype('<i2').tobytes())
mix_wav.close()

